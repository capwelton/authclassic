��          \      �       �      �      �      �      �      �           )  ?  B     �     �     �     �     �      �  "                                          Classic authentication Log in Login Password The login is mandatory. The password is mandatory. You are now logged in... Project-Id-Version: AuthClassic
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-07-13 13:53+0200
PO-Revision-Date: 2017-07-13 13:53+0200
Last-Translator: Paul de Rosanbo <paul.derosanbo@cantico.fr>
Language-Team: Cantico <support@cantico.fr>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: iso-8859-1
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: AuthClassic_translate;AuthClassic_translate:1,2
X-Generator: Poedit 2.0.2
X-Poedit-SearchPath-0: programs
 Authentification classique Se connecter Identifiant Mot de passe L'identifiant est obligatoire Le mot de passe est obligatoire. Vous êtes maintenant connecté... 