; <?php/*
[general]
name				="AuthClassic"
version				="0.1.3"
addon_type			="EXTENSION"
encoding			="UTF-8"
mysql_character_set_database="latin1,utf8"
description			="Module d'authentification login/password."
longdesc			="Module d'authentification login/password utilisant widgets et supportant les requetes ajax pour une utilisation dans un dialog."
addon_access_control=0
delete				=1
db_prefix			="AuthClassic_"
ov_version			="8.4.95"
php_version			="5.4.0"
mysql_version		="4.0"
author				="cantico"
icon				="icon.png"


[addons]
widgets				="1.0.102"
;*/?>