<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';


/**
 * Translates the string.
 *
 * @param string $str
 * @return string
 */
function AuthClassic_translate($str, $str_plurals = null, $number = null)
{
    if ($translate = \bab_functionality::get('Translate/Gettext')) {
    /* @var $translate Func_Translate_Gettext */
        $translate->setAddonName('AuthClassic');

        return $translate->translate($str, $str_plurals, $number);
    }

    return $str;
}

/**
 * Translates all the string in an array and returns a new array.
 *
 * @param array $arr
 * @return array
 */
function AuthClassic_translateArray($arr)
{
    $newarr = $arr;

    foreach ($newarr as &$str) {
        $str = AuthClassic_translate($str);
    }
    return $newarr;
}



/**
 * @return AuthClassic_Controller
 */
function AuthClassic_Controller()
{
    require_once dirname(__FILE__) . '/controller.class.php';
    return bab_getInstance('AuthClassic_Controller');
}



function AuthClassic_getUrl()
{
    $url = bab_rp('referer');
    // verify and buid url
    $params = array();
    $arr = explode('?',$url);

    if (isset($arr[1])) {
        $params = explode('&',$arr[1]);
    }

    $params[] = 'babHttpContext=restore';
    
    $url = $GLOBALS['babPhpSelf'];

    foreach($params as $key => $param) {
        $arr = explode('=',$param);
        if (2 == count($arr)) {

            $params[$key] = $arr[0].'='.$arr[1];
        } else {
            unset($params[$key]);
        }
    }

    if (0 < count($params)) {
        $url .= '?'.implode('&',$params);
    }

    $url = str_replace("\n",'', $url);
    $url = str_replace("\r",'', $url);
    $url = str_replace('%0d','', $url);
    $url = str_replace('%0a','', $url);

    return $url;
}
